Color_Off='\033[0m'       # Text Reset

# Regular Colors
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan

set -e

echo -e "$Purple \n Check versions of services.. $Color_Off"

node -v
npm-v

echo -e "$Green Services has been installed, let's go to run test now my friend :)$Color_Off"

echo -e "$Purple Copy .env.example file... $Color_Off"

cd ..
cp .env.example .env

echo -e "$Green Copy .env.example file did. $Color_Off"

#echo -e "$Purple Run composer commands... $Color_Off"
#
#composer update -q > /dev/null
#composer install -q > /dev/null
#
#echo -e "$Green Composer commands did. $Color_Off"

#echo -e "$Purple Run migration with the seeder... $Color_Off"
#
#php artisan migrate:fresh --seed > /dev/null
#
#echo -e "$Green Migration with the seeder did. $Color_Off"

echo -e "$Purple Run backend server of this project... $Color_Off"

php artisan serve&

echo -e "$Green Server is set. $Color_Off"

echo -e "$Purple Run npm commands... $Color_Off"

npm -v
#npm install > /dev/null

echo -e "$Green Server is set. $Color_Off"
